pycity\_scheduling package
==========================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   pycity_scheduling.algorithms
   pycity_scheduling.case_studies
   pycity_scheduling.classes
   pycity_scheduling.data
   pycity_scheduling.examples
   pycity_scheduling.testing
   pycity_scheduling.util

Submodules
----------

pycity\_scheduling.constants module
-----------------------------------

.. automodule:: pycity_scheduling.constants
   :members:
   :show-inheritance:
   :undoc-members:

pycity\_scheduling.exceptions module
------------------------------------

.. automodule:: pycity_scheduling.exceptions
   :members:
   :show-inheritance:
   :undoc-members:

pycity\_scheduling.solvers module
---------------------------------

.. automodule:: pycity_scheduling.solvers
   :members:
   :show-inheritance:
   :undoc-members:

Module contents
---------------

.. automodule:: pycity_scheduling
   :members:
   :show-inheritance:
   :undoc-members:
