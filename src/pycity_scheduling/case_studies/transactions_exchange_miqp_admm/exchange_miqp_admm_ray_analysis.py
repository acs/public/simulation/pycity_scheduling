"""
The pycity_scheduling framework


Copyright (C) 2025,
Institute for Automation of Complex Power Systems (ACS),
E.ON Energy Research Center (E.ON ERC),
RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""


from pycity_scheduling.classes import *
from pycity_scheduling.algorithms import *
import pycity_scheduling.util.factory as factory
from pycity_scheduling.util.metric import self_consumption, autarky, calculate_co2
from pycity_scheduling.util.write_schedules import schedule_to_json, schedule_to_csv
import pandas as pd
import numpy as np
import os


def main():
    print("\n\n------ Exchange MIQP ADMM Analysis ------\n\n")
    # First, create an environment using the factory's "generate_standard_environment" method. The environment
    # automatically encapsulates time, weather, and price data/information.
    env = factory.generate_standard_environment(initial_date=(2018, 3, 10), step_size=600, op_horizon=24*6)

    # TABULA building stock distribution:
    sfh_distribution = {
        'SFH.1200': 0.033,
        'SFH.1860': 0.096,
        'SFH.1919': 0.112,
        'SFH.1949': 0.085,
        'SFH.1958': 0.150,
        'SFH.1969': 0.145,
        'SFH.1979': 0.070,
        'SFH.1984': 0.11,
        'SFH.1995': 0.102,
        'SFH.2002': 0.077,
        'SFH.2010': 0.01,
        'SFH.2016': 0.01,
    }

    # 50% of households are equipped with an electro-thermal heat pump and 50% with a combined heat and power unit:
    sfh_heating_distribution = {
        'HP': 0.7,
        'BL': 0.0,
        'EH': 0.0,
        'CHP': 0.3,
    }

    # All households are equipped with a fixed load.
    # Moreover, all buildings are also equipped with a photovoltaics and battery storage system (except if buildings
    # possess a CHP unit, see below).
    sfh_device_probs = {
        'FL': 1.0,
        'DL': 0.0,
        'EV': 0.0,
        'BAT': 1.0,
        'PV': 1.0,
    }

    num_sfh_list = [25, 50, 75, 100, 125, 150, 175, 200, 225, 250]
    rho_list = ["varying"]

    # Iterate over all rho values
    for rho in rho_list:
        # Initialize results file path
        path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "results_admm_rho_" + str(rho))
        if not os.path.exists(path):
            os.makedirs(path)

        # Iterate over the number of prosumers
        for num_sfh in num_sfh_list:
            print("\n\nRunning the scenario for rho=" + str(rho) + " and N=" + str(num_sfh) + " SFH\n", flush=True)

            # Create the desired city district using the factory's "generate_tabula_district" method. The district
            # operator's objective is defined as "self-consumption" and the buildings' objectives are defined as
            # "co2". The former objective is quadratic and therefore the optimization problem becomes an MIQP.
            district = factory.generate_tabula_district(environment=env,
                                                        number_sfh=num_sfh,
                                                        number_mfh=0,
                                                        sfh_building_distribution=sfh_distribution,
                                                        sfh_heating_distribution=sfh_heating_distribution,
                                                        sfh_device_probabilities=sfh_device_probs,
                                                        district_objective='co2',
                                                        building_objective='self-consumption',
                                                        seed=0)

            # Set the hot water buffer tank to an approx. volume of 80 liters and add minimum off-/on-time constraints
            # for heating units:
            for building in district.get_lower_entities():
                for entity in building.get_lower_entities():
                    for device in entity.get_lower_entities():
                        if isinstance(device, ThermalHeatingStorage):
                            device.e_th_max = device.e_th_max / 2.0
                            device.capacity = device.e_th_max / device.c_water / 35 * 3.6e6
                            device.th_loss_coeff = (device.k_losses / device.capacity / device.c_water *
                                                    device.time_slot * 3600)
                        if isinstance(device, HeatPump) or isinstance(device, CombinedHeatPower):
                            device.min_off_time = 3
                            device.min_on_time = 2
                            device.lower_activation_limit = 1.0

            # For all buildings with CHP units, remove the photovoltaic units and battery storage systems:
            for building in district.get_lower_entities():
                for entity in building.get_lower_entities():
                    if isinstance(entity, BuildingEnergySystem):
                        if entity.has_chp:
                            entity.has_pv = False
                            entity.has_battery = False
                            entity.pv_units = []
                            entity.battery_units = []

            # Set the parameters for the Exchange MIQP ADMM algorithm:
            max_iterations = 50000
            mode = 'integer'
            if str(rho) == "varying":
                rho_ = 10000.0
                varying_penalty_parameter = True
            else:
                rho_ = rho
                varying_penalty_parameter = False

            # Perform the city district scheduling using the Exchange MIQP ADMM algorithm:
            opt = ExchangeMIQPADMMRay(city_district=district, mode=mode, x_update_mode='constrained',
                                      max_iterations=max_iterations, rho=rho_, eps_exch_primal=5.0,
                                      eps_exch_dual=5.0, varying_penalty_parameter=varying_penalty_parameter,
                                      tau_incr=2.0, tau_decr=2.0, mu=5.0, gamma=1.0, gamma_incr=1.01,
                                      ray_cpu_count=num_sfh+1)

            results = opt.solve()
            district.copy_schedule("integer")

            print("")
            print("Exchange MIQP ADMM algorithm for N=" + str(num_sfh) + " SFH completed.\n", flush=True)
            print("Runtime: {: >4.1f}sec".format(results["times"][-1]))
            print("")

            # Print schedules:
            print("Aggregator schedule:")
            print(list(district.p_el_schedule))
            print("")

            print("Building schedules:")
            district_p_el_net_schedule = np.zeros(len(district.p_el_schedule))
            for building in district.get_lower_entities():
                district_p_el_net_schedule = np.add(district_p_el_net_schedule, building.p_el_schedule)
                print(building, list(building.p_el_schedule))
            print("")

            print("Delta schedule:")
            district_p_el_delta_schedule = np.add(district_p_el_net_schedule, -district.p_el_schedule)
            print(list(district_p_el_delta_schedule))
            print("")

            # Calculate and print the objective value and metrics:
            obj_value = 0.0
            prices = district.environment.prices.co2_prices
            prices = prices[district.op_slice]
            s = sum(abs(prices))
            if s > 0:
                prices = prices * district.op_horizon / s
                for t in range(district.op_horizon):
                    obj_value += prices[t] * max(district.p_el_schedule[t]+district_p_el_delta_schedule[t], 0.0)  # co2
            for bd in district.get_lower_entities():
                for t in range(district.op_horizon):
                    obj_value += min(bd.p_el_schedule[t], 0.0) * min(bd.p_el_schedule[t], 0.0)  # self-consumption
            print("\n\nObjective value: ", obj_value)
            print("Autarky rate: {: >4.2f}%".format(calculate_autarky(district, district.p_el_schedule +
                                                                      district_p_el_delta_schedule) * 100.0))
            print("CO2 emissions [kg]:", calculate_co2_emissions(district, district.p_el_schedule +
                                                                 district_p_el_delta_schedule) / 1000.0)

            # Write results to Excel:
            writer_results_integer = pd.ExcelWriter(
                os.path.join(path, 'exchange_miqp_admm_integer_results_N_' + str(num_sfh) + '.xlsx')
            )

            writer_schedule_integer = pd.ExcelWriter(
                os.path.join(path, 'exchange_miqp_admm_integer_schedule_N_' + str(num_sfh) + '.xlsx')
            )

            results.pop("times")
            df_results_integer = pd.DataFrame(results)
            df_results_integer.to_excel(
                writer_results_integer,
                sheet_name="N=" + str(num_sfh)
            )

            results_schedule_integer = {"district_p_el_schedule": district.p_el_schedule,
                                        "district_p_el_net_schedule": district_p_el_net_schedule,
                                        "district_p_el_delta_schedule": district_p_el_delta_schedule}

            df_results_schedule = pd.DataFrame(results_schedule_integer)
            df_results_schedule.to_excel(
                writer_schedule_integer,
                sheet_name="N=" + str(num_sfh)
            )

            writer_results_integer.close()
            writer_schedule_integer.close()

            # Save the scheduling results to file:
            entities = list(district.get_all_entities())
            schedule_to_csv(entities,
                            file_name=os.path.join(path,
                                "detailed_results_exchange_miqp_admm_integer_N_" + str(num_sfh) + ".csv"),
                            schedule=["integer"])
            schedule_to_json(entities,
                             file_name=os.path.join(path,
                                "detailed_results_exchange_miqp_admm_integer_N_"  + str(num_sfh) + ".json"),
                             schedule=["integer"])
    return


def calculate_autarky(entity, current_schedule=None, timestep=None):
    """
    Local helper function to calculate the autarky rate for the current schedule.

    Parameters
    ----------
    entity : ElectricalEntity
        The entity to calculate the autarky rate for.
    current_schedule : np.array
        The (entity's) current schedule to calculate the autarky rate for.
    timestep : int, optional
        If specified, calculate metric only to this timestep.

    Returns
    -------
    float :
        Autarky rate.
    """
    from pycity_scheduling import classes

    if timestep is None:
        timestep = len(entity.p_el_schedule)
    if current_schedule is None:
        p = entity.p_el_schedule[:timestep]
    else:
        p = current_schedule[:timestep]
    res_schedule = - sum(e.p_el_schedule[:timestep] for e in classes.filter_entities(entity,
                                                                                     'generation_devices'))
    if not isinstance(res_schedule, np.ndarray):
        return 0
    load = p + res_schedule
    np.clip(load, a_min=0, a_max=None, out=load)
    consumption = sum(load)
    if consumption == 0:
        return 1
    cover = sum(np.minimum(res_schedule, load))
    autarky_val = cover / consumption
    return autarky_val


def calculate_co2_emissions(entity, current_schedule=None, timestep=None):
    """
    Local helper function to calculate CO2 emissions for the current schedule.

    Parameters
    ----------
    entity : OptimizationEntity
        The entity to calculate co2 emissions for.
    current_schedule : np.array
        The (entity's) current schedule to calculate the co2 emissions for.
    timestep : int, optional
        If specified, calculate metric only to this timestep.

    Returns
    -------
    float :
        CO2 emissions in [g].
    """
    if timestep is None:
        timestep = len(entity.p_el_schedule)
    if current_schedule is None:
        schedule = entity.p_el_schedule[:timestep]
    else:
        schedule = current_schedule[:timestep]

    co2_emissions = 0.0
    co2 = entity.environment.prices.co2_prices  # in [g/kWh]
    co2 = co2[entity.op_slice]
    for t in range(entity.op_horizon):
        co2_emissions += co2[t] * max(schedule[t], 0.0) * entity.time_slot
    return co2_emissions


if __name__ == '__main__':
    # Run the case study:
    main()
