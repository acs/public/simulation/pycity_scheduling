"""
The pycity_scheduling framework


Copyright (C) 2025,
Institute for Automation of Complex Power Systems (ACS),
E.ON Energy Research Center (E.ON ERC),
RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""


from pycity_scheduling.classes import *
from pycity_scheduling.algorithms import *
import pycity_scheduling.util.factory as factory

import numpy as np
import pandas as pd
import os


def main():
    print("\n\n------ Derivative-free ALADIN parametric study ------\n\n")
    # First, create an environment using the factory's "generate_standard_environment" method. The environment
    # automatically encapsulates time, weather, and price data/information.
    env = factory.generate_standard_environment(initial_date=(2018, 3, 15), step_size=900, op_horizon=96*7)

    # TABULA building stock distribution:
    sfh_distribution = {
        'SFH.1200': 0.033,
        'SFH.1860': 0.096,
        'SFH.1919': 0.112,
        'SFH.1949': 0.085,
        'SFH.1958': 0.150,
        'SFH.1969': 0.145,
        'SFH.1979': 0.070,
        'SFH.1984': 0.11,
        'SFH.1995': 0.102,
        'SFH.2002': 0.077,
        'SFH.2010': 0.01,
        'SFH.2016': 0.01,
    }

    # 50% of households are equipped with an electro-thermal heat pump and 50% with a combined heat and power unit:
    sfh_heating_distribution = {
        'HP': 1.0,
        'BL': 0.0,
        'EH': 0.0,
        'CHP': 0.0,
    }

    # All households are equipped with a fixed load.
    # Moreover, all buildings without CHP unit are also equipped with a photovoltaics and battery storage system.
    sfh_device_probs = {
        'FL': 1.0,
        'DL': 0.0,
        'EV': 0.0,
        'BAT': 1.0,
        'PV': 1.0,
    }

    # Initiate parameter lists over which to iterate
    num_sfh_list = [10, 50, 100]
    Hessian_list = [0.1, 1.0, 10.0]
    alpha_list = [0.25, 0.5, 0.75, 1.0]
    rho_list = [0.05, 0.5, 5, 50]

    # Initialize excel path
    path = os.path.dirname(os.path.abspath(__file__))

    # Iterate over the number of prosumers
    for num_sfh in num_sfh_list:
        
        # Finally, create the desired city district using the factory's "generate_tabula_district" method. The district
        # operator's objective is defined as "peak-shaving" and the buildings' objectives are defined as
        # "price". The former objective is quadratic and therefore the optimization problem becomes an MIQP.
        district = factory.generate_tabula_district(environment=env,
                                                    number_sfh=num_sfh,
                                                    number_mfh=0,
                                                    sfh_building_distribution=sfh_distribution,
                                                    sfh_heating_distribution=sfh_heating_distribution,
                                                    sfh_device_probabilities=sfh_device_probs,
                                                    district_objective='peak-shaving',
                                                    building_objective='price',
                                                    seed=0)

        # Adjust the hot water buffer tank to an approx. volume of 40 liters:
        for building in district.get_lower_entities():
            for entity in building.get_lower_entities():
                for device in entity.get_lower_entities():
                    if isinstance(device, ThermalHeatingStorage):
                        device.e_th_max = device.e_th_max / 4.0
                        device.capacity = device.e_th_max / device.c_water / 35 * 3.6e6
                        device.th_loss_coeff = device.k_losses / device.capacity / device.c_water * device.time_slot * 3600

        # Set the parameters for the centralized reference benchmark algorithm:
        mode = 'convex'

        # Iterate over the hessian scaling parameter, alpha, and rho
        for hessian_val in Hessian_list:
            for alpha_val in alpha_list:
                for rho_val in rho_list:

                    print("#####\n")
                    print("N: ", num_sfh)
                    print("Hessian scaling: ", hessian_val)
                    print("Alpha: ", alpha_val)
                    print("Rho: ", rho_val)
                    print("\n#####")
                    print("", flush=True)

                    # Check if the scenario has been already computed earlier:
                    if os.path.isfile(os.path.join(path, 'ALADIN_results_iter_N_' + str(num_sfh) + '_Hessian_' +
                                                         str(hessian_val) + '_alpha_' + str(alpha_val) + '_rho_' +
                                                         str(rho_val) + '.xlsx')) and \
                        os.path.isfile(os.path.join(path, 'ALADIN_results_schedule_N_' + str(num_sfh) + '_Hessian_' +
                                                         str(hessian_val) + '_alpha_' + str(alpha_val) + '_rho_' +
                                                         str(rho_val) + '.xlsx')):
                        print("Results files already exist - skipping scenario!\n", flush=True)
                    else:
                        # Perform the city district scheduling using the ALADIN algorithm:
                        opt = DerivativeFreeALADINRay_Hvet(city_district=district, mode=mode, max_iterations=2000,
                                                           hessian_scaling=hessian_val, alpha=alpha_val, rho=rho_val,
                                                           eps_primal=0.01, eps_dual=0.01)
                        results = opt.solve()

                        results.pop("times")

                        # Initialize excel writer
                        writer_iter = pd.ExcelWriter(
                            os.path.join(path, 'ALADIN_results_iter_N_' + str(num_sfh) + '_Hessian_' +
                                         str(hessian_val) + '_alpha_' + str(alpha_val) + '_rho_' + str(rho_val) +
                                         '.xlsx')
                        )

                        writer_schedule = pd.ExcelWriter(
                            os.path.join(path, 'ALADIN_results_schedule_N_' + str(num_sfh) + '_Hessian_' +
                                         str(hessian_val) + '_alpha_' + str(alpha_val) + '_rho_' + str(rho_val) +
                                         '.xlsx')
                        )

                        df_results = pd.DataFrame(results)
                        df_results.to_excel(
                            writer_iter,
                            sheet_name="N=" + str(num_sfh) + ";H=" + str(hessian_val) + ";A=" + str(alpha_val) +
                                       ";R=" + str(rho_val)
                        )

                        # Evaluate schedules:
                        district_net_schedule = np.zeros(len(district.p_el_schedule))
                        for bd in district.get_lower_entities():
                            district_net_schedule = np.add(district_net_schedule, bd.p_el_schedule)

                        results_schedule = {"District_schedule": district_net_schedule}

                        df_results_schedule = pd.DataFrame(results_schedule)
                        df_results_schedule.to_excel(
                            writer_schedule,
                            sheet_name="N=" + str(num_sfh) + ";H=" + str(hessian_val) + ";A=" + str(alpha_val) +
                                       ";R=" + str(rho_val)
                        )

                        writer_iter.close()
                        writer_schedule.close()

    return


if __name__ == '__main__':
    # Run the case study:
    main()
