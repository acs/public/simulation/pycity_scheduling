pycity_scheduling
=================

.. image:: ../logos/pycity_scheduling.png
   :scale: 15%
   :align: right


The pycity_scheduling documentation v1.4.1

.. toctree::
   :maxdepth: 3

   pycity_scheduling.algorithms
   pycity_scheduling.classes
   pycity_scheduling.util


