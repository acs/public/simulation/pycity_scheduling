#!/bin/bash

# Name of conda environment
ENV_NAME="pycity_scheduling_env"

# Activate the environment
echo "Activate the Conda environment $ENV_NAME..."
source "$(conda info --base)/etc/profile.d/conda.sh"
conda activate $ENV_NAME

# Start JupyterLab
echo "Start JupyterLab..."
conda run -n $ENV_NAME jupyter lab --notebook-dir=./../../src/pycity_scheduling

# Get the PID of the JupyterLab process
JUPYTER_PID=$!

# Wait for the user to close the terminal or press a key
echo "Press [Ctrl+C] to stop JupyterLab."
wait $JUPYTER_PID

# When the script is interrupted, kill the JupyterLab process
echo "Shutting down JupyterLab..."
kill $JUPYTER_PID
