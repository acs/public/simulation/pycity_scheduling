@echo off
setlocal

:: Name of conda environment
set ENV_NAME=pycity_scheduling_env

:: Activate the environment
echo Activating the Conda environment %ENV_NAME%...
call conda activate %ENV_NAME%

:: Start JupyterLab in the background
echo Start JupyterLab...
start /b jupyter lab --notebook-dir=.\..\..\src\pycity_scheduling

:: Wait for the user to close the console window
pause

:: Kill JupyterLab process when the window is closed
echo Shutting down JupyterLab...
taskkill /f /im jupyter-lab.exe >nul 2>nul

endlocal