#!/bin/bash

# Name of conda environment
ENV_NAME="pycity_scheduling_env"

# Check whether conda is installed
if ! command -v conda &> /dev/null
then
    echo "Conda was not found. Please install Conda first..."
	exit 1 
else
    echo "Conda was found."
fi

# Check whether the Conda environment exists
if conda env list | grep -q "$ENV_NAME"; then
    # If the environment exists, ask the user whether it should be recreated or used
    echo "The Conda environment ‘$ENV_NAME’ already exists."
    while true; do
        read -p "Do you want to use the existing environment (y/n)? (Otherwise it will be recreated): " choice
        case "$choice" in
            y|Y ) 
                echo "Use the existing environment."
                break
                ;;
            n|N )
                echo "Recreate the environment..."
                conda remove --name $ENV_NAME --all -y
                break
                ;;
            * ) 
                echo "Invalid input. Please enter ‘y’ or ‘n’."
                ;;
        esac
    done
else
    echo "The Conda environment ‘$ENV_NAME’ does not exist."
fi

# Create a new Conda environment
if ! conda env list | grep -q "$ENV_NAME"; then
    echo "Create Conda environment $ENV_NAME..."
    conda create -n $ENV_NAME python=3.10 -y
fi

# Activate the environment
echo "Activate the Conda environment $ENV_NAME..."
source "$(conda info --base)/etc/profile.d/conda.sh"
conda activate $ENV_NAME

# Install pycity_scheduling and SCIP solver
echo "Install pycity_scheduling and SCIP solver..."
python -m pip install --upgrade pip setuptools
conda install conda-forge::scip -y
grep -v "mpi4py" ../../requirements.txt | python -m pip install --no-cache-dir -r /dev/stdin
conda install -c conda-forge mpi4py mpich -y
python -m pip install --use-pep517 --no-cache-dir -e ../../.

# Install JupyterLab and IPython Kernel
echo "Install JupyterLab and IPython Kernel..."
conda install jupyterlab ipykernel ipywidgets -y

# Register the kernel for JupyterLab
echo "Register the kernel for JupyterLab..."
python -m ipykernel install --user --name=$ENV_NAME --display-name "Python ($ENV_NAME)"

# Start JupyterLab
echo "Start JupyterLab..."
conda run -n $ENV_NAME jupyter lab --notebook-dir=./../../src/pycity_scheduling

# Get the PID of the JupyterLab process
JUPYTER_PID=$!

# Wait for the user to close the terminal or press a key
echo "Press [Ctrl+C] to stop JupyterLab."
wait $JUPYTER_PID

# When the script is interrupted, kill the JupyterLab process
echo "Shutting down JupyterLab..."
kill $JUPYTER_PID
