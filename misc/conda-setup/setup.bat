@echo off
setlocal

:: Name of conda environment
set ENV_NAME=pycity_scheduling_env

:: Check if conda is installed
where conda >nul 2>nul
if %ERRORLEVEL% NEQ 0 (
    echo Conda was not found. Please install Conda first...
    exit /b 1
) else (
    echo Conda was found.
)

:: Create a new Conda environment
echo Creating Conda environment %ENV_NAME%...
call conda remove --name %ENV_NAME% --all -y
call conda create -n %ENV_NAME% python=3.10 -y

:: Activate the environment
echo Activating the Conda environment %ENV_NAME%...
call conda activate %ENV_NAME%

:: Install pycity_scheduling and SCIP solver
echo Installing pycity_scheduling and SCIP solver...
python -m pip install --upgrade pip setuptools
call conda install conda-forge::scip -y
for /f "delims=" %%i in ('findstr /v "mpi4py" ..\..\requirements.txt') do (
    echo %%i
    python -m pip install --no-cache-dir %%i
)
call conda install -c conda-forge mpi4py mpich -y
python -m pip install --use-pep517 --no-cache-dir -e ..\..\.

:: Install JupyterLab and IPython Kernel
echo Installing JupyterLab and IPython Kernel...
call conda install jupyterlab ipykernel ipywidgets -y

:: Register the kernel for JupyterLab
echo Registering the kernel for JupyterLab...
python -m ipykernel install --user --name=%ENV_NAME% --display-name "Python (%ENV_NAME%)"

:: Start JupyterLab in the background
echo Start JupyterLab...
start /b jupyter lab --notebook-dir=.\..\..\src\pycity_scheduling

:: Wait for the user to close the console window
pause

:: Kill JupyterLab process when the window is closed
echo Shutting down JupyterLab...
taskkill /f /im jupyter-lab.exe >nul 2>nul

endlocal