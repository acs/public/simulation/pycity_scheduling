# README - Conda and JupyterLab Setup Script

## Overview
This script sets up a Conda environment, installs JupyterLab, and provides necessary files. There are two versions of the script:
- **setup.sh** (for Linux/macOS)
- **setup.bat** (for Windows)

After setup, JupyterLab is started. When the window is closed, the JupyterLab server is automatically shut down.

---

## Usage on Linux/macOS
### Requirements:
- **Bash** (default on Linux/macOS)
- **Conda** (Anaconda or Miniconda must be installed)

### Steps:
1. **Make the script executable** (if necessary):
   ```sh
   chmod +x setup.sh
   ```
2. **Run the script:**
   ```sh
   ./setup.sh
   ```

If Conda is not found, ensure it is included in your `PATH` variable.

---

## Usage on Windows
### Requirements:
- **Windows CMD or PowerShell**
- **Conda** (Anaconda or Miniconda must be installed and available in `PATH`)

### Steps:
1. **Double-click `setup.bat`**, or alternatively:
   - **Run manually** (if errors occur):
     ```cmd
     setup.bat
     ```

If Conda is not found, ensure the Conda path has been added to the environment variables (see README above).

---

## What does the script do?
- Checks if Conda is installed.
- Creates or activates the Conda environment `my_jupyterlab_environment`.
- Installs JupyterLab and IPython Kernel.
- Copies files from the `files` folder to the current directory.
- Starts JupyterLab.
- Shuts down the JupyterLab server when the window is closed.

---

## Troubleshooting
- **Conda not found?**
  - Check if Conda is installed with `conda --version`.
  - Add the Conda path to the environment variables.
- **JupyterLab not starting?**
  - Ensure the Conda environment is correctly activated.
  - Check the console output for errors.

If further issues arise, review the console output and ensure all dependencies are correctly installed.

